// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class CGProject : ModuleRules
{
	public CGProject(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "ShaderCore", "RenderCore", "RHI", "RuntimeMeshComponent" });

		PrivateDependencyModuleNames.AddRange(new string[] {  });
        
        PublicIncludePaths.AddRange(new string[] {"ProceduralMeshComponent/Public", "ProceduralMeshComponent/Classes"});

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
