// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "CPTBlueprintFunctionLibrary.generated.h"

UCLASS()
class CGPROJECT_API UCPTBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category = "custom")
		static TArray<FVector> calculateVerticesFromDensities(TArray<float> densities);
	UFUNCTION(BlueprintCallable, Category = "custom")
		static float densityFunction(FVector pos);
	 
};
