// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class CGPROJECT_API DataClass
{
public:
	static TArray<TArray<FIntVector>> EdgeTable;
	static TArray<int32> TrianglesToCase;
	DataClass();
	~DataClass();
	static void ReadTheMainTable();
	static void ReadTheEdgeTable();
	static TArray<FIntVector> GetEdgesOnCase(int32 caseNumber);
	static int32 GetTrianglesOnCase(int32 caseNumber);
};
