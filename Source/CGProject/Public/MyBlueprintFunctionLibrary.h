// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "MyBlueprintFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class CGPROJECT_API UMyBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{

	GENERATED_BODY()
	public:
		
	UFUNCTION(BlueprintCallable, Category = "custom")
		static bool FileSaveString(FString SaveTextB, FString FileNameB);

	UFUNCTION(BlueprintPure, Category = "custom")
		static bool FileLoadString(FString FileNameA, FString& SaveTextA);

	UFUNCTION(BlueprintCallable, Category = "custom")
		static TArray<FVector> VerticesByEdge(TArray<FVector>& cube, TArray<float>& densities, int32 caseNumber);

	UFUNCTION(BlueprintCallable, Category = "custom")
		static uint8 CalculateCaseNumber(TArray<float>& densities);

	UFUNCTION(BlueprintCallable, Category = "custom")
		static void InitData();

	UFUNCTION(BlueprintCallable, Category = "custom")
		static TArray<FIntVector> GetEdgesOnCase(int32 caseNumber);

	UFUNCTION(BlueprintCallable, Category = "custom")
		static int32 GetTrianglesOnCase(int32 caseNumber);



	
};

