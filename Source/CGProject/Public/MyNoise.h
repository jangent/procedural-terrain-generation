// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class CGPROJECT_API MyNoise
{
public:
	TArray<TArray<TArray<float>>> Texture3D;
	float sample(float x, float y, float z);
	float fmod(float x, int32 y);
	float h;
	float w;
	float l;
	MyNoise(uint32 height, uint32 width, uint32 length);
	MyNoise();
	~MyNoise();
};
