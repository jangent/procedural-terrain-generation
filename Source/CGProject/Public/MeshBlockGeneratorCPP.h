// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "RuntimeMeshComponent.h"
#include "MyNoise.h"
#include "MeshBlockGeneratorCPP.generated.h"

UCLASS()
class CGPROJECT_API AMeshBlockGeneratorCPP : public AActor
{
	MyNoise octave1;
	MyNoise octave2;
	MyNoise octave3;
	MyNoise octave4;
	MyNoise octave5;
	MyNoise octave6;
	MyNoise octave7;
	MyNoise octave8;
	MyNoise octave9;
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMeshBlockGeneratorCPP();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	virtual void OnConstruction(const FTransform& Transform) override;

	uint32 getKey(uint8 k, uint8 j, uint8 i, uint8 edgeNumber);

	void createBlock(int32 id, FVector origin, uint8 voxels, float voxelEdge);

	void createOrigins();

	FVector calcNormal(FVector pos);

	float densityFunction(FVector pos);
		
	static uint8 CalculateCaseNumber(TArray<float>& densities);

	static void InitData();

	static TArray<FIntVector> GetEdgesOnCase(int32 caseNumber);

	static int32 GetTrianglesOnCase(int32 caseNumber);

	URuntimeMeshComponent *rmc; 

	float minVoxelEdge; //voxel edge for closest blocks

	uint8 maxVoxels; //voxels in block edge for closest blocks

	float blockEdge; //Block edge length

	uint8 rounds; //in total 2**rounds cubes will be created

	int32 inds[8];

	TArray<FVector> origins;

	int32 atOrigin;

	UMaterial* TheMaterial;


	
};
