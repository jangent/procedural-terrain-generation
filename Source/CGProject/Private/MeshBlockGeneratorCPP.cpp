 // Fill out your copyright notice in the Description page of Project Settings.

#include "CGProject.h"
#include "MeshBlockGeneratorCPP.h"
#include "DataClass.h"
//#include "CPTBlueprintFunctionLibrary.h"
//#include "MyBlueprintFunctionLibrary.h"


// Sets default values
AMeshBlockGeneratorCPP::AMeshBlockGeneratorCPP() : inds{ 0, 4, 6, 2, 1, 5, 7, 3 },
octave1(32, 32, 32),
octave2(24, 24, 24),
octave3(16, 16, 16),
octave4(8, 8, 8),
octave5(16, 16, 16),
octave6(16, 16, 16),
octave7(16, 16, 16),
octave8(16, 16, 16),
octave9(16, 16, 16)
{

	//PrimaryActorTick.TickInterval = 10.0;
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//PrimaryActorTick.TickGroup = TG_PostPhysics;

	minVoxelEdge = 20.0; //initial value
	maxVoxels = 30; //initial value
	blockEdge = maxVoxels * minVoxelEdge;
	rounds = 12;

	USphereComponent* SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
	RootComponent = SphereComponent;
	rmc = CreateDefaultSubobject<URuntimeMeshComponent>(TEXT("GeneratedMesh"));

	rmc->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	static ConstructorHelpers::FObjectFinder<UMaterial> Material(TEXT("Material'/Game/VertexColor.VertexColor'"));

	if (Material.Object != NULL)
	{
		TheMaterial = (UMaterial*)Material.Object;
	}
}

// Called when the game starts or when spawned
void AMeshBlockGeneratorCPP::BeginPlay()
{
	Super::BeginPlay();
	createOrigins();
	atOrigin = 0;
}

// Called every frame
void AMeshBlockGeneratorCPP::Tick(float DeltaTime)
{
	if (atOrigin < origins.Num()) {
		uint8 vox = maxVoxels; //FMath::Max(30, FMath::CeilToInt(maxVoxels / FMath::Max(1.0f, origins[atOrigin].Size() / blockEdge / 4.0f))); //furher from player means less voxels
		createBlock(atOrigin, origins[atOrigin], vox, blockEdge / (float)vox);
		atOrigin++;
	}
	else {
		PrimaryActorTick.bCanEverTick = false;
	}
	Super::Tick(DeltaTime);
}

void AMeshBlockGeneratorCPP::OnConstruction(const FTransform& Transform) {

	Super::OnConstruction(Transform);
	InitData();//sort of really important if you don't want UE to crash

	uint8 vox = 30;
	createBlock(0, FVector(0, 0, 0), vox, blockEdge / vox);
	createBlock(1, FVector(0, 0, blockEdge), vox, blockEdge / vox);
}

uint32 AMeshBlockGeneratorCPP::getKey(uint8 k, uint8 j, uint8 i, uint8 edgeNumber) {
	uint32 key = k;
	key <<= 8;
	key |= j;
	key <<= 8;
	key |= i;
	key <<= 8;
	key |= edgeNumber;
	if (key < 0) {
		UE_LOG(LogTemp, Warning, TEXT("Wrong %d, %d, %d, %d"), k, j, i, edgeNumber);
	}
	return  key;
}

float AMeshBlockGeneratorCPP::densityFunction(FVector pos) {
	float density = -pos[2] + blockEdge;
	density += octave1.sample(pos[0], pos[1], pos[2]) * 2;
	density += octave2.sample(pos[0] * 0.2, pos[1] * 0.2, pos[2] * 0.2) * minVoxelEdge/2;
	if (pos[2] > blockEdge) {
		density += octave3.sample(pos[0] * 0.004, pos[1] * 0.004, pos[2] * 0.004) * blockEdge/4;
		density += octave3.sample(pos[0] * 0.002, pos[1] * 0.002, pos[2] * 0.002) * blockEdge/4;
	}
	else {
		density += octave3.sample(pos[0] * 0.004, pos[1] * 0.004, pos[2] * 0.004) * blockEdge ;
		density += octave3.sample(pos[0] * 0.002, pos[1] * 0.002, pos[2] * 0.002) * blockEdge ;
	}
	density += octave4.sample(pos[0] * 0.0005, pos[1] * 0.0005, pos[2] * 0.0005) * blockEdge * 2;
	//density += octave1.sample(pos[0]/blockEdge * 2 , pos[1]/blockEdge * 2, pos[2]/blockEdge * 2) * blockEdge*2;
	//density += -octave2.sample(pos[0] * 0.001 + 7, pos[1] * 0.001 + 5, pos[2] * 0.001 + 2);
	//density = -FMath::Clamp(octave2.sample(pos[0] * 0.001 + 5, pos[1] * 0.001 + 5, pos[2] * 0.001 + 5) + 0.0f,0.0f,1.0f);
	//float wx = octave3.sample(pos[0] * 0.001, pos[1] * 0.001, pos[2] * 0.001);
	//float wy = octave5.sample(pos[0] * 0.001, pos[1] * 0.001, pos[2] * 0.001);
	//float wz = octave6.sample(pos[0] * 0.001, pos[1] * 0.001, pos[2] * 0.001); 

	/*if (pos[0] < 0 && pos[0] > -1000) {
		if (pos[1] < 0 && pos[1] > -1000){
			density = - pos[2] + blockEdge;
		}
	}*/

	//density += octave1.sample(wx, wy, wz) * 100 + 40;
	//float hard_floor_y = 13;
	if (pos[2] >= blockEdge*2 && density > 0) {
		density = -0.0001;
	}
	//density = pos[2] >= 1350 ? 1350-pos[2] : density;
	if (pos[2] == 0.0) {
		density = 0;
	}
	//density = pos[2] <= 5 ? 5-pos[2] : density;
	//density += FMath::Clamp((hard_floor_y - pos.Y) * 3,0.0f,1.0f) * 40;
	//+ octave3.sample(pos[0] * 0.001,pos[1] * 0.002, pos[2] * 0.002)//FMath::RandRange(-10.0f, 10.0f) + 10;
	return density;
}

void AMeshBlockGeneratorCPP::createBlock(int32 id, FVector origin, uint8 voxels, float voxelEdge) {
	
	TArray<int32> triangles;
	TArray<FVector> vertices;
	TArray<FVector> Normals;
	TArray<FRuntimeMeshTangent> Tangents;
	TArray<FColor> VertexColors;

	TArray<FVector2D> TextureCoordinates;
	TArray<TArray<TArray<float>>> densityVolume;
	densityVolume.Reserve(voxels);
	TMap<uint32, int32> vertPlaceInArray;


	for (uint8 i = 0; i < voxels; i++) { //z
		for (uint8 j = 0; j < voxels; j++) { //y
			for (uint8 k = 0; k < voxels; k++) { //x

												 //----------------------------------filling densityVolume-------------------------

				TArray<float> densities;
				densities.Reserve(8);
				TSet<uint8> edgesNeeded; //on which edges vertices may be generated

				if (i + k + j == 0) { //very first cube
					TArray<TArray<float>> layer1;
					layer1.Reserve(voxels);
					TArray<float> row1;
					TArray<float> row2;
					row1.Reserve(voxels);
					row2.Reserve(voxels);
					layer1.Add(row1); //create first row of densities
					layer1.Add(row2); //create next row of the same layer
					densityVolume.Add(layer1); //create first layer

					TArray<TArray<float>> layer2;
					layer2.Reserve(voxels);
					TArray<float> row3;
					TArray<float> row4;
					row3.Reserve(voxels);
					row4.Reserve(voxels);
					layer2.Add(row3); // create row above
					layer2.Add(row4); // create row behind that
					densityVolume.Add(layer2); //create layer above

											   //make first cube
					for (int z = 0; z < 2; z++) {
						for (int y = 0; y < 2; y++) {
							for (int x = 0; x < 2; x++) {
								FVector pos((x * voxelEdge), (y * voxelEdge), (z * voxelEdge));
								densityVolume[z][y].Add(densityFunction(origin + pos));
							}
						}
					}

					for (uint8 temp = 0; temp < 12; temp++)
						edgesNeeded.Add(temp); //need vertices on all the edges

                    //empty
                    row1.Empty();
					row2.Empty();
					layer1.Empty();
					row3.Empty();
					row4.Empty();
					layer2.Empty();
				}
				else {
					if (k == 0) { //first cube of the row
						edgesNeeded.Add(1);
						edgesNeeded.Add(2);

						if (j == 0) { //first cube of the layer
							TArray<TArray<float>> layer;
							layer.Reserve(voxels);
							TArray<float> row1;
							TArray<float> row2;
							row1.Reserve(voxels);
							row2.Reserve(voxels);
							layer.Add(row1); // create row above
							layer.Add(row2); // create row behind that
							densityVolume.Add(layer); //create layer above

													  //upper front corners
							for (int x = 0; x < 2; x++) {
								FVector pos((x * voxelEdge), j, ((i + 1) * voxelEdge));
								densityVolume[i + 1][j].Add(densityFunction(origin + pos));
							}
							edgesNeeded.Add(0);
							edgesNeeded.Add(9);
							edgesNeeded.Add(4);
                            
                            //empty
                            row1.Empty();
							row2.Empty();
							layer.Empty();
						}

						else if (i == 0) { //first in row on bottom layer
							TArray<float> row1;
							TArray<float> row2;
							row1.Reserve(voxels);
							row2.Reserve(voxels);
							densityVolume[0].Add(row1); //create next row of the same layer
							densityVolume[1].Add(row2); // create row above

							
														//bottom back corners
							for (int x = 0; x < 2; x++) {
								FVector pos((x * voxelEdge), ((j + 1)*voxelEdge), 0);
								densityVolume[0][j + 1].Add(densityFunction(origin + pos));
							}
							edgesNeeded.Add(3);
							edgesNeeded.Add(7);
							edgesNeeded.Add(11);
                            
                            //empty
                            row1.Empty();
							row2.Empty();
						}
						else {
							//create row above
							TArray<float> row1;
							row1.Reserve(voxels);
							densityVolume[i + 1].Add(row1);

							row1.Empty();
						}

						//left back top corner 
						FVector pos(0, ((j + 1)*voxelEdge), ((i + 1)*voxelEdge));
						densityVolume[i + 1][j + 1].Add(densityFunction(origin + pos));

					}

					else if (j == 0) { //first in column
						edgesNeeded.Add(9);
						edgesNeeded.Add(4);

						if (i == 0) { //bottom layer
									  //bottom right corners
							for (int y = 0; y < 2; y++) {
								FVector pos(((k + 1)*voxelEdge), (y * voxelEdge), 0);
								densityVolume[0][y].Add(densityFunction(origin + pos));
							}
							edgesNeeded.Add(8);
							edgesNeeded.Add(7);
							edgesNeeded.Add(11);
						}

						//right front top corner
						FVector pos(((k + 1)*voxelEdge), 0, ((i + 1)*voxelEdge));
						densityVolume[i + 1][0].Add(densityFunction(origin + pos));
					}

					else if (i == 0) { //first layer
									   //bottom right back corner
						FVector pos(((k + 1)*voxelEdge), ((j + 1)*voxelEdge), 0);
						densityVolume[0][j + 1].Add(densityFunction(origin + pos));
						edgesNeeded.Add(7);
						edgesNeeded.Add(11);
					}

					//everyone needs a right back top corner 
					FVector pos(((k + 1)*voxelEdge), ((j + 1)*voxelEdge), ((i + 1)*voxelEdge));
					densityVolume[i + 1][j + 1].Add(densityFunction(origin + pos));
					edgesNeeded.Add(5);
					edgesNeeded.Add(6);
					edgesNeeded.Add(10);
				}
				//-----------------------------------densities in right order-------------------

				densities.Add(densityVolume[i][j][k]);
				densities.Add(densityVolume[i + 1][j][k]);
				densities.Add(densityVolume[i + 1][j + 1][k]);
				densities.Add(densityVolume[i][j + 1][k]);
				densities.Add(densityVolume[i][j][k + 1]);
				densities.Add(densityVolume[i + 1][j][k + 1]);
				densities.Add(densityVolume[i + 1][j + 1][k + 1]);
				densities.Add(densityVolume[i][j + 1][k + 1]);

				uint8 caseNumber = CalculateCaseNumber(densities);

				//-----------------------------------------making real vertices-----------------------
				if (caseNumber != 0 && caseNumber != 255) {
					//calculating vertex coordinates for the cube (does some extra work)
					TArray<FVector> cube;
					cube.Reserve(8);

					cube.Add(origin + FVector(k * voxelEdge, j * voxelEdge, i * voxelEdge));
					cube.Add(origin + FVector(k * voxelEdge, j * voxelEdge, (i + 1) * voxelEdge));
					cube.Add(origin + FVector(k * voxelEdge, (j + 1) * voxelEdge, (i + 1) * voxelEdge));
					cube.Add(origin + FVector(k * voxelEdge, (j + 1) * voxelEdge, i * voxelEdge));
					cube.Add(origin + FVector((k + 1) * voxelEdge, j * voxelEdge, i * voxelEdge));
					cube.Add(origin + FVector((k + 1) * voxelEdge, j * voxelEdge, (i + 1) * voxelEdge));
					cube.Add(origin + FVector((k + 1) * voxelEdge, (j + 1) * voxelEdge, (i + 1) * voxelEdge));
					cube.Add(origin + FVector((k + 1) * voxelEdge, (j + 1) * voxelEdge, i * voxelEdge));

					TSet<uint8> done; //done from needed
					done.Reserve(15);
					//tabelist
					TArray<FIntVector> triangleList = GetEdgesOnCase(caseNumber);
					uint8 numOfTriangles = GetTrianglesOnCase(caseNumber);
					for (uint8 tri = 0; tri < numOfTriangles; tri++) {
						FIntVector triangle = triangleList[tri];
						for (uint8 e = 0; e <3; e++) {
							uint8 edgeNumber = triangle[e];
							uint32 key = -1;
							if (edgesNeeded.Contains(edgeNumber)) { //belongs to this cube
								key = getKey(k, j, i, edgeNumber);
								if (!done.Contains(edgeNumber)) {
									uint8 vert1;
									uint8 vert2;
									if (edgeNumber < 8)
										vert2 = edgeNumber;
									else
										vert2 = edgeNumber % 8;
									if (edgeNumber == 3 || edgeNumber == 7)
										vert1 = edgeNumber - 3;
									else if (edgeNumber > 7)
										vert1 = edgeNumber - 4;
									else
										vert1 = 1 + edgeNumber;
									
									FVector pos(cube[vert1] + (cube[vert2] - cube[vert1]) * densities[vert1] / (densities[vert1] - densities[vert2]));
									vertices.Add(pos);
                                    Normals.Add(calcNormal(pos));
									VertexColors.Add(FColor(100 * (pos[2] / blockEdge) + 55, 120 * (pos[2] / blockEdge) + 15, 20, 255));
	                                Tangents.Add(FRuntimeMeshTangent(0, -1, 0));

                                    TextureCoordinates.Add(FVector2D(0, 0));
									vertPlaceInArray.Add(key, vertices.Num() - 1);
									done.Add(edgeNumber);
								}
							}

							else { //belongs to neighbour
								switch (edgeNumber) {
								case 0:
									if (k == 0)
										key = getKey(k, j - 1, i, 2);
									else if (j == 0)
										key = getKey(k - 1, j, i, 4);
									else
										key = getKey(k - 1, j - 1, i, 6);
									break;
								case 1:
									key = getKey(k - 1, j, i, 5);
									break;
								case 2:
									key = getKey(k - 1, j, i, 6);
									break;
								case 3:
									if (k == 0)
										key = getKey(k, j, i - 1, 1);
									else if (i == 0)
										key = getKey(k - 1, j, i, 7);
									else
										key = getKey(k-1, j, i - 1, 5);
									break;
								case 4:
									key = getKey(k, j - 1, i, 6);
									break;
								case 7:
									key = getKey(k, j, i - 1, 5);
									break;
								case 8:
									if (i == 0)
										key = getKey(k, j - 1, i, 11);
									else if (j == 0)
										key = getKey(k, j, i - 1, 9);
									else
										key = getKey(k, j - 1, i - 1, 10);
									break;
								case 9:
									key = getKey(k, j - 1, i, 10);
									break;
								case 11:
									key = getKey(k, j, i - 1, 10);
									break;
								}
							}
							if (!vertPlaceInArray.Contains(key)) {
								UE_LOG(LogTemp, Warning, TEXT("\nMissing %d, k=%d j=%d i=%d eNr=%d"), key, k, j, i, edgeNumber);
								UE_LOG(LogTemp, Warning, TEXT("Triangle %d, %d, %d"), triangle[0], triangle[1], triangle[2]);

							}
							else if (vertPlaceInArray[key] < 0 || vertPlaceInArray[key] >= vertices.Num()) {
								UE_LOG(LogTemp, Warning, TEXT("wrong %d, %d"), key, vertPlaceInArray[key]);
							}
							else
								triangles.Insert(vertPlaceInArray[key], 0);

						}
					}
                    cube.Empty();
                    done.Empty();
				}
				densities.Empty();
			}
		}
	}
	rmc->CreateMeshSection(id, vertices, triangles, Normals, TextureCoordinates, VertexColors, Tangents, false, EUpdateFrequency::Infrequent);
	rmc->SetMaterial(id, TheMaterial);

	densityVolume.Empty();
    triangles.Empty();
    vertices.Empty();
	Normals.Empty();
	Tangents.Empty();
	VertexColors.Empty();
	TextureCoordinates.Empty();
	
}

void AMeshBlockGeneratorCPP::createOrigins() {
	origins.Reserve(rounds * rounds * 4 );
	for (int8 round = 1; round <= rounds; round++) {
		for (int8 x = -round; x < round; x++) {
			for(int8 z = 0; z < 2; z++){
				origins.Add(FVector(x, round - 1, z) * blockEdge); //upper edge
				origins.Add(FVector(x, -round, z) * blockEdge); //lower edge
			}
		}

		for (int8 y = 1-round; y < round-1; y++) {//corners were done last cycle 
			for (int8 z = 0; z < 2; z++) {
				origins.Add(FVector(-round, y, z) * blockEdge); //left edge
				origins.Add(FVector(round - 1, y, z) * blockEdge); //right edge
			}
		} 
	}
}

uint8 AMeshBlockGeneratorCPP::CalculateCaseNumber(TArray<float>& densities) {
	uint8 densityCase = 0;
	for (int32 i = 7; i >= 0; i--) {
		densityCase <<= 1;
		densityCase |= (densities[i] >= 0.0f);
	}
	return densityCase;
}

void AMeshBlockGeneratorCPP::InitData() {
	DataClass d;
}

TArray<FIntVector> AMeshBlockGeneratorCPP::GetEdgesOnCase(int32 caseNumber) {
	return DataClass::GetEdgesOnCase(caseNumber);
}

int32 AMeshBlockGeneratorCPP::GetTrianglesOnCase(int32 caseNumber) {
	return DataClass::GetTrianglesOnCase(caseNumber);
}

FVector AMeshBlockGeneratorCPP::calcNormal(FVector pos) {
	float d = 0.000001;
	FVector normal;
	normal[0] = densityFunction(pos + FVector(d, 0, 0)) -
		densityFunction(pos + FVector(-d, 0, 0));
	normal[1] = densityFunction(pos + FVector(0, d, 0)) -
		densityFunction(pos + FVector(0, -d, 0));
	normal[2] = densityFunction(pos + FVector(0, 0, d)) -
		densityFunction(pos + FVector(0, 0, -d));
	if (normal.Normalize(0.0001)) {
		return -normal;
	}
	return FVector(0,0,1);
}