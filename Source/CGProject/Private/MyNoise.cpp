// Fill out your copyright notice in the Description page of Project Settings.

#include "CGProject.h"
#include "MyNoise.h"

float MyNoise::sample(float x, float y, float z){
	x = fmod(x, l) < 0 ? l + fmod(x, l) : fmod(x, l);
	y = fmod(y, w) < 0 ? w + fmod(y, w) : fmod(y, w);
	z = fmod(z, h) < 0 ? h + fmod(z, h) : fmod(z, h);
	int32 x2 = (int32)FMath::RoundFromZero(x) < l ? (int32)FMath::RoundFromZero(x) : 0;
	int32 x1 = (int32)(x) < l ? (int32)(x) : 0;
	int32 y2 = (int32)FMath::RoundFromZero(y) < w ? (int32)FMath::RoundFromZero(y) : 0;
	int32 y1 = (int32)(y) < w ? (int32)(y) : 0;
	int32 z2 = (int32)FMath::RoundFromZero(z) < h ? (int32)FMath::RoundFromZero(z) : 0;
	int32 z1 = (int32)(z) < h ? (int32)(z) : 0;

	float xl1 = FMath::Lerp(Texture3D[z1][y1][x1], Texture3D[z1][y1][x2], x - x1);
	float xl2 = FMath::Lerp(Texture3D[z1][y2][x1], Texture3D[z1][y2][x2], x - x1);
	float xyl1 = FMath::Lerp(xl1, xl2, y - y1);

	float xl3 = FMath::Lerp(Texture3D[z2][y1][x1], Texture3D[z2][y1][x2], x - x1);
	float xl4 = FMath::Lerp(Texture3D[z2][y2][x1], Texture3D[z2][y2][x2], x - x1);
	float xyl2 = FMath::Lerp(xl3, xl4, y - y1);

	return FMath::Lerp(xyl1, xyl2, z - z1);
}

float MyNoise::fmod(float x, int32 y){
	int32 base = (int32)x;
	float left = x - base;
	int32 mod = base % y;
	return mod + left;
}

MyNoise::MyNoise(uint32 height, uint32 width, uint32 length) : h(height), w(width), l(length){
	Texture3D.Reserve(h);
	for (uint32 i = 0; i < h; i++) {
		TArray<TArray<float>> layer;
		layer.Reserve(w);
		Texture3D.Add(layer);
		for (uint32 j = 0; j < w; j++) {
			TArray<float> row;
			row.Reserve(l);
			Texture3D[i].Add(row);
			for (uint32 k = 0; k < l; k++) {
				//if(i == 0)
					Texture3D[i][j].Add(FMath::RandRange(-1.0f, 1.0f));
				//else Texture3D[i][j].Add(-1.0);
			}
		}
	}
}

MyNoise::MyNoise()
{
}

MyNoise::~MyNoise()
{
}
