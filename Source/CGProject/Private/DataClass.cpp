// Fill out your copyright notice in the Description page of Project Settings.

#include "CGProject.h"
#include "DataClass.h"
#include "AssertionMacros.h"

TArray<TArray<FIntVector>> DataClass::EdgeTable = TArray<TArray<FIntVector>>();
TArray<int32> DataClass::TrianglesToCase = TArray<int32>();

DataClass::DataClass()
{
	DataClass::ReadTheMainTable();
	DataClass::ReadTheEdgeTable();
}

DataClass::~DataClass()
{
}

void  DataClass::ReadTheMainTable() {
	FString tableAsString = "";
	FString filePath = FPaths::GameDir() + "Content/asd.txt";
	FFileHelper::LoadFileToString(tableAsString, *filePath);
	for (int32 i = 0; i < 256; i++) {
		TrianglesToCase.Add((int)tableAsString[i] - 48);
	}
}
void  DataClass::ReadTheEdgeTable() {
	FString tableAsString = "";
	FString filePath = FPaths::GameDir() + "Content/tabel2.txt";
	FFileHelper::LoadFileToString(tableAsString, *filePath);

	int32 counter = 0;
	for (int32 i = 0; i < 256; i++) {
		TArray<FIntVector> thisCase = TArray<FIntVector>();
		for (int32 j = 0; j < 5; j++) {
			FIntVector edges = FIntVector();
			for (int32 k = 0; k < 3; k++) {
				if (tableAsString[counter] == _T('-')) {
					edges[k] = -1;
				}
				else if (tableAsString[counter] == _T('A')) {
					edges[k] = 10;
				}
				else if (tableAsString[counter] == _T('B')) {
					edges[k] = 11;
				}
				else {
					edges[k] = (int)tableAsString[counter] - 48;
				}
				counter++;
			}
			thisCase.Add(edges);
		}
		EdgeTable.Add(thisCase);
	}

}

TArray<FIntVector> DataClass::GetEdgesOnCase(int32 caseNumber) {
	return EdgeTable[caseNumber];
}
int32 DataClass::GetTrianglesOnCase(int32 caseNumber) {
	return TrianglesToCase[caseNumber];
}