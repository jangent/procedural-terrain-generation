// Fill out your copyright notice in the Description page of Project Settings.

#include "CGProject.h"
#include "MyBlueprintFunctionLibrary.h"
#include "DataClass.h"

bool UMyBlueprintFunctionLibrary::FileSaveString(FString SaveTextB, FString FileNameB)
{
	return FFileHelper::SaveStringToFile(SaveTextB, *(FPaths::GameDir() + FileNameB));
}

bool UMyBlueprintFunctionLibrary::FileLoadString(FString FileNameA, FString& SaveTextA)
{
	return FFileHelper::LoadFileToString(SaveTextA, *(FPaths::GameDir() + FileNameA));
}

TArray<FVector> UMyBlueprintFunctionLibrary::VerticesByEdge(TArray<FVector>& cube, TArray<float>& densities, int32 caseNumber)
{
	int32 triangles = GetTrianglesOnCase(caseNumber);
	TArray<FIntVector> edges = GetEdgesOnCase(caseNumber);
	TArray<FVector> newVerts = TArray<FVector>();
	for (int32 i = 0; i < triangles; i++) {
		FIntVector edge = edges[i];
		for (int32 j = 0; j < 3; j++) {
			int32 edgeNumber = edge[j];
			int32 vert1;
			int32 vert2;
			if (edgeNumber < 8)
				vert1 = edgeNumber;
			else 
				vert1 = edgeNumber % 8;
			if (edgeNumber == 3 || edgeNumber == 7) 
				vert2 = edgeNumber - 3;
			else if (edgeNumber > 7) 
				vert2 = edgeNumber - 4;
			else 
				vert2 = 1 + edgeNumber;
			
			newVerts.Add(cube[vert1] + (cube[vert2] - cube[vert1]) * densities[vert1] / (densities[vert1] - densities[vert2]));
		}
	}
	return newVerts;
}


uint8 UMyBlueprintFunctionLibrary::CalculateCaseNumber(TArray<float>& densities) {
	uint8 densityCase = 0;
	for (int32 i = 7; i >= 0; i--) {
		densityCase <<= 1; 
		densityCase |= (densities[i]>=0.0f);
	}
	return densityCase;
}

void UMyBlueprintFunctionLibrary::InitData() {
	DataClass d;
}

TArray<FIntVector> UMyBlueprintFunctionLibrary::GetEdgesOnCase(int32 caseNumber) {
	return DataClass::GetEdgesOnCase(caseNumber);
}

int32 UMyBlueprintFunctionLibrary::GetTrianglesOnCase(int32 caseNumber) {
	return DataClass::GetTrianglesOnCase(caseNumber);
}

