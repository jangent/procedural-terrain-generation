# README #

### What is this repository for? ###

* This is the repo for our Computer Graphics project
* More info can be found [here](https://courses.cs.ut.ee/2016/cg/fall/Main/Project-ProceduralTerrains)

### How do I get set up? ###

* We currently don't have a good method for downloading executables, but it will come soon.
* There is no need for extra configuration at the moment
* Dependencies: UE4, preferably 4.14
* To run our small demo project, you first need to clone the repo and open it in UE4. This will prompt you to build the required .dll-s (we try to only include source code and basic configuration in our repo). After that you can open the level testingGround, which can be found in the Content folder. This currently contains our most important and working classes/blueprints.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact